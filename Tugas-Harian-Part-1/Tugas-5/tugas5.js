// Soal 1
console.log('-----soal 1-----')
function halo() {
  return "Halo Sanbers!"
}

console.log(halo()) // "Halo Sanbers!" 

console.log()

// Soal 2
console.log('-----soal 2-----')
function kalikan(x, y) {
  return x * y
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48
console.log()

// Soal 3

console.log('-----soal 3-----')
function introduce(name, age, address, hobby) {
  var kalimat = "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
  return kalimat
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
console.log()

// soal 4
console.log("----SOAL 4-----")
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]

var objectPeserta = {
  nama: arrayDaftarPeserta[0],
  jenisKelamin: arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  tahunLahir: arrayDaftarPeserta[3]
}

console.log(objectPeserta)
console.log()

//soal 5
console.log("----SOAL 5-----")
var buahBuah = [
  { nama: "strawberry",
    warna: "merah",
    adaBijinya: false,
    harga: 9000 
  },
  { nama: "jeruk",
    warna: "oranye",
    adaBijinya: true,
    harga: 8000
  },
  { nama: "Semangka",
    warna: "Hijau & Merah",
    adaBijinya: true,
    harga: 10000
  },
  { nama: "Pisang",
    warna: "Kuning",
    adaBijinya: false,
    harga: 5000
  }
]

console.log(buahBuah[0])
console.log()

// soal 6
console.log("----SOAL 6-----")

var dataFilm = []

function tambahDataFilm(nama, durasi, genre, tahun){
  dataFilm.push({
    nama: nama,
    durasi: durasi,
    genre: genre,
    tahun: tahun
  })
}

tambahDataFilm("LOTR", "2 jam", "action", "1999")
tambahDataFilm("avenger", "2 jam", "action", "2019")
tambahDataFilm("spiderman", "2 jam", "action", "2004")
tambahDataFilm("juon", "2 jam", "horror", "2004")

console.log(dataFilm)
console.log()

