# Quiz Week 3 - ReactJS Sanbercode

mohon di perhatikan untuk mengikuti instruksi yang di berikan, masing-masing soal sudah di perjelas menggunakan apa yang diinginkan pada jawaban terima kasih
------------

## 1. setup (5 poin)
buatlah folder baru pada repository tugas harian dengan nama "Quiz-3" sejajar dengan "Tugas-Harian-Part-1" dan "Tugas-Harian-Part-2" yang berisi project reactjs yang sudah ada package axios dan react-router-dom

## 2. convert dari html ke jsx (10 poin)
ubahlah index.html dari Tugas-2 menjadi jsx (bukan berarti rubah extensi tapi rubah semua isinya menjadi jsx) ke dalam reactjs beserta dengan css dan gambarnya

anda bisa menggunakan Tugas-2 anda sendiri atau menggunakan kode yang ada di https://gitlab.com/sanbercode-reactjs/jawaban-tugas-bootcamp-batch18-part1/-/tree/master/Tugas-2

## 3. router (15 poin)
ubahlah bagian body tersebut menjadi switch yang isinya adalah route dari tiap component yang akan di gunakan
dan ubahlah nav link itu sesuai dengan component yang nantinya akan di buat untuk nav menu. nav menu sendiri terdiri dari:
- Home
- About
- Books List Editor (hanya muncul jika user login)
- Login (Jika user belum login)

![quiz3-1.jpg](quiz3-1.jpg?raw=true)

## 4. about component (5 poin)
ubahlah about.html dari Tugas-2 menjadi jsx ke dalam reactjs

gunakan function component soal ini

## 5. home component (15)
home component ini berisi daftar book yang berisi detail masing-masing berupa judul, gambar, Tahun Terbit, jumlah halaman, harga, deskripsi dan ulasan, dan untuk gambar agar di sesuaikan standar width dan heightnya agar rapi

![quiz3-1.jpg](quiz3-1.jpg?raw=true)

gunakan class component dan axios untuk soal ini (untuk url API bisa menggunakan yang ada di crud)

## 6. crud buku component(25)

![quiz3-2.jpg](quiz3-2.jpg?raw=true)

![quiz3-3.jpg](quiz3-3.jpg?raw=true)

buatlah CRUD book yang berisi form book, table book beserta delete dan edit. lalu buatlah fitur search diatasnya yang berfungsi untuk mencari film berdasarkan nama

untuk atribut yang dimiliki oleh book diantaranya adalah:
- title(string)
- description(textarea)
- review(textarea)
- release_year(integer)- defaultnya 2020 dan minimal tahun 1980
- totalPage(integer)
- price (integer)
- image_url(string)

semua input required
pastikan validasi inputnya disesuaikan dengan atribut diatas (misal release_year minimal di isi dengan 1980)

berikut ini url API yang digunakan dalam CRUD books ini: 
```php
GET http://backendexample.sanbercloud.com/api/books

POST http://backendexample.sanbercloud.com/api/books

GET http://backendexample.sanbercloud.com/api/books/{ID_BOOKS}

PUT http://backendexample.sanbercloud.com/api/books/{ID_BOOKS}

DELETE http://backendexample.sanbercloud.com/api/books/{ID_BOOKS}
```

gunakan hooks dan axios untuk mengerjakan soal ini (tambahkan styling pada tabel dan form)

## 7. Login (25)
buatlah fitur login dengan ketentuan user yang belum login tidak dapat membuka halaman book list editor
dan menu book list editor tidak akan muncul dan tidak akan bisa di akses jika user belum login

untuk username dan password yang di gunakan adalah
username: admin
password: admin

jika user memasukkan username dan password yang salah maka munculkan alert

gunakan context untuk mengerjakan soal ini

## Hasil yang diinginkan
berikut ini link video yang menampilkan hasil yang diinginkan dalam quiz ini:
https://www.youtube.com/watch?v=wUBwpFJolX4



## Kumpulkan Quiz
quiz di kumpulkan dengan mengirimkan link commit terakhir ke web sanbercode, untuk teknis pengumpulannya sama seperti quiz biasa, buka web sanbercode dan buka quiz 3 dan isi link commit (tidak mengumpulkan dalam link commit terakhir akan berimbas pada pengurangan 10 poin)

PERLU DIINGAT buka quiz 3 di web sanbercode sebaiknya ketika ingin di kumpulkan saja




